#include <iostream>
#include <string>

using namespace std;


template <typename T>
class Stack
{
private:
	T* array;
	int arrayCount;
	int arraySize;
	int step = 5;//����������, ������� ����������� ������ � ����������� size �� ��� ����� 

public:
	Stack(int myArraySize) 
	{
		arraySize = myArraySize;
		array = new T[arraySize];
		arrayCount = 0;
	}
	
	void push(T value) 
	{
		if (arrayCount >= arraySize - 1) 
		{
			outOfOrderCopySol();
		}
		array[arrayCount] = value;
		arrayCount++;
	}

	T pop() 
	{
		if (arrayCount == 0) return array[arrayCount];
		return array[arrayCount-1];

	}

	//������� �� ����� � ������� �� �����
	T remove() 
	{
		T removeItem= array[arrayCount];
		array[arrayCount]=NULL;
		arrayCount--;
		return removeItem;
	}

	//������� ����������� ������� � ����� ������ � ���������������� ����������
	void outOfOrderCopySol()
	{

		T *newArray = new T[arraySize + step];
		for (int i = 0; i < arraySize - 1; i++) 
		{
			newArray[i] = array[i];
		}
		delete[] array;
		array = newArray;
		arraySize += step;
		
	}

	//������������ �����, �������� ������� �� �����������������
	// ( �������� ��������� ����� ������������ outOfOrderCopySol() ) 
	
	void test() 
	{
		for (int i = 0; i < arraySize - 1; i++)
		{
			cout << array[i] << endl;
		}
	}

	
	void del() {
		delete[] array;
	}

};

int main()
{
	Stack<double>* stack = new Stack<double>(2);
	stack->push(26.6);
	stack->push(15.2);
	stack->push(23.3);
	stack->push(23.4);
	cout<<stack->pop();
	
	stack->test();
	
	stack->del();
	
}

